# Amplette

_Amplette was created using the lettering template that came with
the Poem Edition pamphlet _The material discovery of the alphabet_
by Éloïsa Pérez.
_Amplette_ is an anagram of _template_.

First made on paper, then hand–eye digitised using Glyphs Mini.

The shape of the _S_ in the template (the template provides only part of
the _S_, about ⅔) is particularly subtle and difficult for me to capture
with scan-and-trace (which i have not done).

The template provides a sort of kit of elements from which letters can be made.
It's a bit like making a glyph in an OpenType font by composing components.

The template is intended to be used for the upper case.
These were first drawn on paper using the template:

<figure>
![The regular upper case alphabet in a random order](plaque-amelt-regular-upper.png "Amplette Regular upper case alphabet")
<figcaption>
Amplette Regular upper case
</figcaption>
</figure>

I also created, entirely digitally but using the same components,
a lower case set:

<figure>
![The regular lower case alphabet in a random order](plaque-amelt-regular-lower.png "Amplette Regular lower case alphabet")
</figure>
<figcaption>
Amplette Regular lower case
</figcaption>

Then i went a little bit off-piste and created an uncial alphabet.
Specifically the _a_, _e_, _f_, _g_, _m_, _t_ have particularly uncial forms,
the rest are a bit of a mixture.

<figure>
![The uncial alphabet in a random order](plaque-amelt-uncial.png "Amplette Uncial alphabet")
<figcaption>
Amplette Uncial
</figcaption>
</figure>


## Progress

- [x] Latin core, good draft of drawings
- [x] numbers, reasonable drawings
- [ ] punctuation, minimal
- [ ] spacing, basic draft; requires refinement


## Design notes: Regular edition

- cap-height: 750
- x-height: 550 (73.3%)
- ascender: 800 (trial)

The _d_ is cheeky.
It is a "circle plus stick", but
the stick has been pushed left at the same time as
removing the right-hand part of the circle.
It creates a D-shaped counter.

The _u_ is very wide, as a result of the geometric design.

The _f_ has a cross-stroke only on the right, and it is vertically
positioned below the x-height.
Both decisions questionable.

The _b_ has no foot, eliminating the notch.
This is to avoid it looking like a rotated _d_.
But it means it would be difficult to
draw the shape accurately using a hypothetical template.


## Design notes: Uncial edition

The _j_ has a negative left side-bearing, because of its descender.
This is generally fine, and clashing problems with letters could be fixed
with kerning.
But if you start a line with _j_ then the glyph is clipped.
Is it better to ban negative left side-bearing and use lots of kerns
instead?


# END
